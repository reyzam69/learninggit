console.log("Hello World!");
let variavel = 0;
console.log("Variável " + variavel + " é do tipo: " + typeof(variavel));

let numero1 = 5;
let numero2 = 10;

console.log(numero1+numero2);

console.log(5*3);


try{
    let idade = 40;

    if(idade == undefined){
        console.log("idade não foi criada!");
    }else{
        console.log("Sua idade é: " + idade);
    }
}catch(error){
    console.log("Erro na variável idade: ");
}


function mostrarIdade(){
    let idade = 40;
    if(idade < 41){
        console.log("Jovem");
    }else{
        console.log("Não muito jovem");
    }
}
mostrarIdade();

function mostrarIdade2(vlIdade){
    if(vlIdade < 42){
         console.log(vlIdade + " - Jovem");
    }else{
        console.log(vlIdade + " - Não muito jovem");
    }
} 
mostrarIdade2(40);

const mostrarIdade3 = (vlIdade) => {
    if(vlIdade < 42){
            console.log(vlIdade + " - Jovem - versão 3");
    }else{
        console.log(vlIdade + " - Não muito jovem - versão 3");
    }
} 
mostrarIdade3(42);



